###############################################
#          Quick mouse configuration          #
#                                             #
# A powershell script to quickly:             #
# - change mouse sensitivity                  #
# - enable or disable the enhanced precision  #
#                                             #
# The script instantly affects your mouse     #
# behavior and saves changes in registry      #
# (the same way as changing settings from     #
# the control pannel would do).               #
###############################################

param (
    [int]$sensitivity = 16,
    [int]$precision = 1
)





# C# function to call Windows API
Add-Type @"
using System;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace Enigma
{
    public class Mouse {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            private static extern int SystemParametersInfo(
                int uAction, 
                int uParm, 
                ref int lpvParam, 
                int fuWinIni);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            private static extern int SystemParametersInfo(
                int uAction, 
                int uParm, 
                int lpvParam, 
                int fuWinIni);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            private static extern int SystemParametersInfo(
                int uAction, 
                int uParm, 
                int[] lpvParam, 
                int fuWinIni);

         [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            private static extern int SystemParametersInfo(
                int uAction, 
                int uParm, 
                ref int[] lpvParam, 
                int fuWinIni);

        private const int SPI_GETMOUSE = 0x0003;
        private const int SPI_SETMOUSE = 0x0004;
        private const int SPI_GETMOUSESPEED = 0x0070;
        private const int SPI_SETMOUSESPEED = 0x0071;
        private const int SPIF_UPDATEINIFILE = 0x01;
        private const int SPIF_SENDCHANGE = 0x02;

        public int[] spiGetMouse() {
            int[] res = new int[3];
            SystemParametersInfo(SPI_GETMOUSE, 0, res, 0);
            return res;
        }

        public int spiSetMouse(int threshold1, int threshold2, int speed) {
            int[] value = {threshold1, threshold2, speed};
            int res = SystemParametersInfo(SPI_SETMOUSE, 0, value, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
            return res;
        }

        public int spiGetMouseSpeed() {
            int res = 0;
            SystemParametersInfo(SPI_GETMOUSESPEED, 0, ref res, 0);
            return res;
        }

        public int spiSetMouseSpeed(int value) {
            int res = SystemParametersInfo(SPI_SETMOUSESPEED, 0, value, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
            return res;
        }
    }
}

"@

$mouse = New-Object Enigma.Mouse





# Function to print settings currently used (API) and saved in register
function printRegisterSettings() {
    Write-Host "Register settings"
    Write-Host "  MouseSensitivity :  $((get-itemProperty 'hkcu:\Control Panel\Mouse').MouseSensitivity)"
    Write-Host "  MouseThreshold1 :  $((get-itemProperty 'hkcu:\Control Panel\Mouse').MouseThreshold1)"
    Write-Host "  MouseThreshold2 :  $((get-itemProperty 'hkcu:\Control Panel\Mouse').MouseThreshold2)"
    Write-Host "  MouseSpeed :  $((get-itemProperty 'hkcu:\Control Panel\Mouse').MouseSpeed)"
}

function printAPISettings() {
    Write-Host "API settings"
    Write-Host "  MouseSensitivity :  $($mouse.spiGetMouseSpeed())"
    $mousePrecision = $mouse.spiGetMouse()
    Write-Host "  MouseThreshold1 before :  $($mousePrecision[0])"
    Write-Host "  MouseThreshold2 before :  $($mousePrecision[1])"
    Write-Host "  MouseSpeed before :  $($mousePrecision[2])"
}

function printAllSettings() {
    printRegisterSettings
    printAPISettings
}





# Print current settings
Write-Host "====== Current settings ======"
printAllSettings

# Set sensitivity
$null = $mouse.spiSetMouseSpeed($sensitivity) #API
set-itemProperty 'hkcu:\Control Panel\Mouse' -name MouseSensitivity -value $sensitivity #Register

# Set enhanced precision
$MOUSE_THRESHOLD1 = 0
$MOUSE_THRESHOLD2 = 0
$MOUSE_SPEED = 0

if ($precision -ne 0) {
    $MOUSE_THRESHOLD1 = 6 
    $MOUSE_THRESHOLD2 = 10
    $MOUSE_SPEED = 1
}

$null = $mouse.spiSetMouse($MOUSE_THRESHOLD1, $MOUSE_THRESHOLD2, $MOUSE_SPEED) #API
set-itemProperty 'hkcu:\Control Panel\Mouse' -name MouseSpeed -value $MOUSE_SPEED #Register
set-itemProperty 'hkcu:\Control Panel\Mouse' -name MouseThreshold1 -value $MOUSE_THRESHOLD1
set-itemProperty 'hkcu:\Control Panel\Mouse' -name MouseThreshold2 -value $MOUSE_THRESHOLD2

# Print new settings
Write-Host
Write-Host "====== New settings ======"
printAllSettings
