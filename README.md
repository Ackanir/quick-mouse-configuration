# Quick mouse configuration
A powershell script to quickly:
- change mouse sensitivity
- enable or disable the enhanced precision

The script instantly affects your mouse behavior and saves changes in registry (the same way as changing settings from the control pannel would do).

# How to use
- Clone this repository or download the `Quick_mouse_configuration.ps1` script.
- Create a new shortcut for every configuration you want to use.
In "Target", set the following command:  
```bash
powershell.exe -noexit -ExecutionPolicy Bypass -File ".\Quick_mouse_configuration.ps1" -sensitivity 16 -precision 0
```

Make sur to set the right path to the _.ps1_ script (relative as in the example or full path).
- Set the `sensitivity` argument to desired value (between 0 and 20).
- Set the `precision` argument to 0 (disabled) or 1 (enabled) to affect the `enhanced precision` setting.

If any argument is ommited, the default value will be applied (`sensitivity` = 16, `enhanced precision` enabled).

# Sources and more information
- Mouse sensivity settings  
    https://renenyffenegger.ch/notes/Windows/PowerShell/examples/WinAPI/modify-mouse-speed  
    https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-systemparametersinfoa  
- Mouse enhanced precision  settings  
    https://gist.github.com/NickCraver/7ebf9efbfd0c3eab72e9  
    https://www.tenforums.com/tutorials/101691-turn-off-enhance-pointer-precision-windows.html  
- Using C# for API calls  
    https://stackoverflow.com/questions/34640103/powershell-using-c-sharp-code-from-add-type-crashes-shell-systemparamtersinfo  

# Registry values
Settings are saved in `Ordinateur\HKEY_CURRENT_USER\Control Panel\Mouse`.
To set or reset your settings, you can refer to the following `keys` and associated values.
## Mouse sensivity  
By default, `MouseSensitivity` = 16
## Mouse enhanced precision  
- If enabled  
    `MouseSpeed` = 1  
    `MouseThreshold1` = 6  
    `MouseThreshold2` = 10  
- If disabled  
    `MouseSpeed` = 0  
    `MouseThreshold1` = 0  
    `MouseThreshold2` = 0  
